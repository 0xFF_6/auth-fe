# Auth-fe

## How to test locally

- Open locally the [auth-service](https://gitlab.com/EolasMedical/backend/auth/auth-service) project
- In the `serverless.ts` file in auth-service change the environment variables `COGNITO_CLIENT_ID` and `USER_POOL_ID` with the ones to be used in the test
- Start auth-service locally on port 4000 (port 3000 will be used by React)
- In the .env file set `REACT_APP_AUTH_SERVICE_BASE_URL=http://localhost:4000/dev/v1` and set `REACT_APP_USER_POOL_ID` and `REACT_APP_USER_POOL_WEB_CLIENT_ID` using the same values used in the auth-service project
- Fill `REACT_APP_DOMAIN`, `REACT_APP_REDIRECT_SIGN_IN` and `REACT_APP_REDIRECT_SIGN_OUT`
- `npm start`
