export const FORM_TYPES = {
  signUp: "signUp",
  confirmSignUp: "confirmSignUp",
  signIn: "signIn",
  signedIn: "signedIn",
  federatedSignIn: "federatedSignIn",
  forgotPassword: "forgotPassword",
  forgotPasswordSubmit: "forgotPasswordSubmit",
  resendSignUp: "resendSignUp",
};

export const dropDownItems = [
  {
    label: "Sign up",
    key: FORM_TYPES.signUp,
  },
  {
    label: "Sign in",
    key: FORM_TYPES.signIn,
  },
  {
    label: "Federated sign in",
    key: FORM_TYPES.federatedSignIn,
  },
  {
    label: "Forgot password",
    key: FORM_TYPES.forgotPassword,
  },
  {
    label: "Resend sign up",
    key: FORM_TYPES.resendSignUp,
  },
];

export const FORM_ITEMS = {
  email: { label: "Email", name: "email" },
  password: { label: "Password", name: "password" },
  givenName: { label: "Given name", name: "givenName" },
  familyName: { label: "Family name", name: "familyName" },
  authCode: { label: "Auth code", name: "authCode" },
  confirmationCode: { label: "Confirmation code", name: "confirmationCode" },
};
