import React, { useState, useRef } from "react";
import SignUpForm from "./forms/SignUpForm.js";
import SignInForm from "./forms/SignInForm.js";
import FederatedSignInForm from "./forms/FederatedSignInForm.js";
import ConfirmSignUpForm from "./forms/ConfirmSignUpForm.js";
import ForgotPasswordForm from "./forms/ForgotPasswordForm.js";
import ForgotPasswordSubmitForm from "./forms/ForgotPasswordSubmitForm.js";
import ResendSignUpForm from "./forms/ResendSignUpForm.js";
import AuthDropdown from "./AuthDropdown.js";
import { Auth, Hub } from "aws-amplify";
import axios from "axios";
import { FORM_TYPES, dropDownItems } from "./constants";

const myAxios = axios.create({
  baseURL: process.env.REACT_APP_AUTH_SERVICE_BASE_URL,
});

export default function App() {
  // STATE
  const [formType, updateFormType] = useState(FORM_TYPES.signUp);
  const [email, setEmail] = useState("");
  const [user, updateUser] = useState(null);

  const firstRenderRef = useRef(true);

  // UTILS
  const checkUser = async () => {
    try {
      const currentAuthenticatedUser = await Auth.currentAuthenticatedUser();
      updateUser(currentAuthenticatedUser);
      updateFormType(FORM_TYPES.signedIn);
    } catch (error) {
      updateUser(null);
    }
  };

  const setAuthListener = async () => {
    Hub.listen("auth", (data) => {
      switch (data.payload.event) {
        case "signOut":
          checkUser();
          updateFormType(FORM_TYPES.signUp);
          break;
        case "signIn":
          checkUser();
          break;
        default:
          break;
      }
    });
  };

  if (firstRenderRef.current) {
    firstRenderRef.current = false;
    checkUser();
    setAuthListener();
  }

  const signUp = async ({ email, password, givenName, familyName }) => {
    try {
      await myAxios.post("/sign-up", {
        email,
        password,
        givenName,
        familyName,
      });
      setEmail(email);
      updateFormType(FORM_TYPES.confirmSignUp);
    } catch (error) {
      console.log("error :>> ", error);
    }
  };

  const confirmSignUp = async ({ authCode }) => {
    await Auth.confirmSignUp(email, authCode);
    updateFormType(FORM_TYPES.signIn);
  };

  const signIn = async ({ email, password }) => {
    try {
      await Auth.signIn(email, password);
      updateFormType(FORM_TYPES.signedIn);
    } catch (error) {
      if (error.code === "400") {
        alert(error.message);
      } else {
        alert("Unknown error");
      }
    }
  };

  const signOut = async () => {
    await Auth.signOut();
  };

  const federatedSignIn = async ({ email }) => {
    try {
      const { data } = await myAxios.get(`/user/idp?email=${email}`);
      await Auth.federatedSignIn({ customProvider: data });
    } catch (error) {
      console.log("error :>> ", error);
    }
  };

  const forgotPassword = async ({ email }) => {
    try {
      await Auth.forgotPassword(email);
      setEmail(email);
      updateFormType(FORM_TYPES.forgotPasswordSubmit);
    } catch (error) {
      if (error.code === "400") {
        alert(error.message);
      } else {
        alert("Unknown error");
      }
    }
  };

  const resendSignUp = async ({ email }) => {
    try {
      await Auth.resendSignUp(email);
      setEmail(email);
      updateFormType(FORM_TYPES.confirmSignUp);
    } catch (error) {
      if (error.code === "400") {
        alert(error.message);
      } else {
        alert("Unknown error");
      }
    }
  };

  const forgotPasswordSubmit = async ({ confirmationCode, password }) => {
    await Auth.forgotPasswordSubmit(email, confirmationCode, password);
    updateFormType(FORM_TYPES.signIn);
  };

  return (
    <div className="App">
      {user === null && (
        <AuthDropdown
          onClick={({ key }) => updateFormType(key)}
          items={dropDownItems}
        />
      )}
      {formType === FORM_TYPES.signUp && <SignUpForm onSignUp={signUp} />}
      {formType === FORM_TYPES.resendSignUp && (
        <ResendSignUpForm onResendSignUp={resendSignUp} />
      )}
      {formType === FORM_TYPES.federatedSignIn && (
        <FederatedSignInForm onFederatedSignIn={federatedSignIn} />
      )}
      {formType === FORM_TYPES.forgotPassword && (
        <ForgotPasswordForm onForgotPassword={forgotPassword} />
      )}
      {formType === FORM_TYPES.forgotPasswordSubmit && (
        <ForgotPasswordSubmitForm
          onForgotPasswordSubmit={forgotPasswordSubmit}
        />
      )}
      {formType === FORM_TYPES.confirmSignUp && (
        <ConfirmSignUpForm onConfirmSignUp={confirmSignUp} />
      )}
      {formType === FORM_TYPES.signIn && <SignInForm onSignIn={signIn} />}
      {formType === FORM_TYPES.signedIn && (
        <div>
          <h1>Hello world, welcome {user?.attributes.given_name}.</h1>
          <button onClick={signOut}>Sign Out</button>
        </div>
      )}
    </div>
  );
}
