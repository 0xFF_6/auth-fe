/* eslint-disable jsx-a11y/anchor-is-valid */
import { Menu, Dropdown, Space } from "antd";
import { DownOutlined } from "@ant-design/icons";

const AuthDropdown = ({ onClick, items }) => {
  const menu = <Menu onClick={onClick} items={items} />;
  return (
    <Dropdown overlay={menu}>
      <a onClick={(e) => e.preventDefault()}>
        <Space>
          Auth dropdown
          <DownOutlined />
        </Space>
      </a>
    </Dropdown>
  );
};

export default AuthDropdown;
