const config = {
  Auth: {
    userPoolId: process.env.REACT_APP_USER_POOL_ID,
    userPoolWebClientId: process.env.REACT_APP_USER_POOL_WEB_CLIENT_ID,
    endpoint: `${process.env.REACT_APP_AUTH_SERVICE_BASE_URL}/proxy`,
  },
  oauth: {
    domain: process.env.REACT_APP_DOMAIN,
    redirectSignIn: process.env.REACT_APP_REDIRECT_SIGN_IN,
    redirectSignOut: process.env.REACT_APP_REDIRECT_SIGN_OUT,
    responseType: "token",
  },
};

export default config;
