import React from "react";
import FormTemplate from "./FormTemplate";
import { FORM_ITEMS } from "../constants";

const ForgotPasswordSubmit = ({ onForgotPasswordSubmit }) => {
  const items = [FORM_ITEMS.confirmationCode, FORM_ITEMS.password];
  return (
    <FormTemplate
      onFinish={onForgotPasswordSubmit}
      buttonText="Forgot password submit"
      items={items}
    />
  );
};
export default ForgotPasswordSubmit;
