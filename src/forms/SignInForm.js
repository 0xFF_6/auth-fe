import React from "react";
import FormTemplate from "./FormTemplate";
import { FORM_ITEMS } from "../constants";

const SignInForm = ({ onSignIn }) => {
  const { email, password } = FORM_ITEMS;
  const items = [email, password];
  return (
    <FormTemplate onFinish={onSignIn} buttonText="Sign in" items={items} />
  );
};
export default SignInForm;
