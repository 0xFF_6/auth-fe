import React from "react";
import { Button, Form, Input } from "antd";

const FormTemplate = ({ onFinish, buttonText, items }) => {
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      {items.map((i, index) => {
        return (
          <Form.Item
            key={index}
            label={i.label}
            name={i.name}
            rules={[
              {
                required: true,
                message: `Please input your ${i.name}!`,
              },
            ]}
          >
            {i.name === "password" ? <Input.Password /> : <Input />}
          </Form.Item>
        );
      })}

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          {buttonText}
        </Button>
      </Form.Item>
    </Form>
  );
};
export default FormTemplate;
