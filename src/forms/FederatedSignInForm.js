import React from "react";
import FormTemplate from "./FormTemplate";
import { FORM_ITEMS } from "../constants";

const FederatedSignInForm = ({ onFederatedSignIn }) => {
  const items = [FORM_ITEMS.email];
  return (
    <FormTemplate
      onFinish={onFederatedSignIn}
      buttonText="Federated Sign in"
      items={items}
    />
  );
};
export default FederatedSignInForm;
