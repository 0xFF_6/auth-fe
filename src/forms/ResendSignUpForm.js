import React from "react";
import FormTemplate from "./FormTemplate";
import { FORM_ITEMS } from "../constants";

const ResendSignUpForm = ({ onResendSignUp }) => {
  const items = [FORM_ITEMS.email];

  return (
    <FormTemplate
      onFinish={onResendSignUp}
      buttonText="Resend sign up"
      items={items}
    />
  );
};
export default ResendSignUpForm;
