import React from "react";
import FormTemplate from "./FormTemplate";
import { FORM_ITEMS } from "../constants";

const ForgotPasswordForm = ({ onForgotPassword }) => {
  const items = [FORM_ITEMS.email];
  return (
    <FormTemplate
      onFinish={onForgotPassword}
      buttonText="Forgot password"
      items={items}
    />
  );
};
export default ForgotPasswordForm;
