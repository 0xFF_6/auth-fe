import React from "react";
import FormTemplate from "./FormTemplate";
import { FORM_ITEMS } from "../constants";

const SignUpForm = ({ onSignUp }) => {
  const { email, password, givenName, familyName } = FORM_ITEMS;

  const items = [email, password, givenName, familyName];
  return (
    <FormTemplate onFinish={onSignUp} buttonText="Sign up" items={items} />
  );
};
export default SignUpForm;
