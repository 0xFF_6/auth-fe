import React from "react";
import FormTemplate from "./FormTemplate";
import { FORM_ITEMS } from "../constants";

const ConfirmSignUpForm = ({ onConfirmSignUp }) => {
  const items = [FORM_ITEMS.authCode];
  return (
    <FormTemplate
      onFinish={onConfirmSignUp}
      buttonText="Confirm sign up"
      items={items}
    />
  );
};
export default ConfirmSignUpForm;
